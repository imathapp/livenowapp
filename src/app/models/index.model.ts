export class MeetingModel {
  id: number;
  // tslint:disable-next-line: variable-name
  app_id: number;
  // tslint:disable-next-line: variable-name
  user_ref: string;
  code: string;
  // tslint:disable-next-line: variable-name
  opentok_session_id: string;
  status: number;
  time: string;
  desc: string;
  name: string;
}

export class SessionModel {
  id: number;
  // tslint:disable-next-line: variable-name
  meeting_id: number;
  // tslint:disable-next-line: variable-name
  user_id: number;
  // tslint:disable-next-line: variable-name
  display_name: string;
  // tslint:disable-next-line: variable-name
  device_info: string;
  // tslint:disable-next-line: variable-name
  time_begin: string;
  // tslint:disable-next-line: variable-name
  time_end: string;
  // tslint:disable-next-line: variable-name
  opentok_token: string;
  // tslint:disable-next-line: variable-name
  opentok_session_id: string;
  screens: Array<number>;
  // tslint:disable-next-line: variable-name
  meeting_id_text: string;
  // tslint:disable-next-line: variable-name
  meeting_status: string;
  // tslint:disable-next-line: variable-name
  meeting_time: string;
  // tslint:disable-next-line: variable-name
  meeting_timezone: string;
}

export class BoardModel {
  id: number;
  width: number;
  height: number;
  json: string;
}

export class ResponseApi {
  success: boolean;
  data: any;
  message: string;
  code: number;
}

export class SessionHistory {
  // tslint:disable-next-line: variable-name
  meeting_id: number;
  // tslint:disable-next-line: variable-name
  date: string;
  code: string;
}