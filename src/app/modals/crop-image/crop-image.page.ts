import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import Cropper from 'cropperjs';
import { Platform, Events, NavParams, ModalController } from '@ionic/angular';
import { LoadingService } from 'src/app/services/common/loading.service';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-crop-image',
  templateUrl: './crop-image.page.html',
  styleUrls: ['./crop-image.page.scss'],
})
export class CropImagePage implements OnInit {
  @ViewChild('image', { static: false }) input: ElementRef;
  imageBase64: any;
  width: number;
  height: number;
  cropper: Cropper;
  cropperOptions: Cropper.Options;
  editType: any;

  constructor(
    private navParams: NavParams,
    private events: Events,
    private loader: LoadingService,
    private modalController: ModalController,
    private screenOrientation: ScreenOrientation,
    private platform: Platform
  ) {
    this.editType = navParams.get('type');
    if (this.editType === 'whiteBoard') {
      this.setLandscape();
    }
    this.cropperOptions = {
      dragMode: 'move',
      guides: false,
      highlight: false,
      background: false,
      movable: true,
      zoomable: true,
      autoCropArea: 1,
      initialAspectRatio: 4 / 3,
      scalable: true,
      minCropBoxWidth: 200,
      minCropBoxHeight: 200,
      crop: (e) => { },
      ready: () => {
        this.cropper.setCropBoxData({
          top: 0,
          width: 300,
          height: 200,
        });
      }
    };
  }

  async setLandscape() {
    return await this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);
  }

  async setPortrait() {
    return await this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ngOnInit() {
    this.loader.present();
    this.events.subscribe('uploadImage', (data) => {
      if (data) {
        this.imageBase64 = data.img;
      }
    });
  }

  cropperLoad() {
    this.cropper = new Cropper(this.input.nativeElement, this.cropperOptions);
    setTimeout(() => {
      this.loader.dismiss();
    }, 1000);
  }

  reset() {
    this.cropper.reset();
  }

  rotate() {
    this.cropper.rotate(90);
  }

  cancel() {
    this.modalController.dismiss();
  }

  finish() {
    const croppedImgB64String: string = this.cropper.getCroppedCanvas({
      maxWidth: 1024,
      maxHeight: 1024,
      imageSmoothingEnabled: false,
    }).toDataURL('image/jpeg');
    this.modalController.dismiss({ imageData: croppedImgB64String });
  }

}
