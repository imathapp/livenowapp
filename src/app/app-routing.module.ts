import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'test', pathMatch: 'full' },
  // { path: '', redirectTo: 'whiteboard', pathMatch: 'full' },
  // { path: '', redirectTo: 'demo', pathMatch: 'full' },
  {
    path: 'crop-image',
    loadChildren: () => import('./modals/crop-image/crop-image.module').then( m => m.CropImagePageModule)
  },
  {
    path: 'new-free-meeting',
    loadChildren: () => import('./pages/new-free-meeting/new-free-meeting.module').then( m => m.NewFreeMeetingPageModule)
  },
  {
    path: 'whiteboard',
    loadChildren: () => import('./pages/whiteboard/whiteboard.module').then( m => m.WhiteboardPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'meet/:id',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./pages/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'new-session',
    loadChildren: () => import('./pages/new-session/new-session.module').then( m => m.NewSessionPageModule)
  },
  
  {
    path: 'startup',
    loadChildren: () => import('./pages/startup/startup.module').then( m => m.StartupPageModule)
  },
  {
    path: 'email-enter',
    loadChildren: () => import('./pages/email-enter/email-enter.module').then( m => m.EmailEnterPageModule)
  },
  {
    path: 'sign-in',
    loadChildren: () => import('./pages/sign-in/sign-in.module').then( m => m.SignInPageModule)
  },
  {
    path: 'sign-up',
    loadChildren: () => import('./pages/sign-up/sign-up.module').then( m => m.SignUpPageModule)
  },
  {
    path: 'email-verify',
    loadChildren: () => import('./pages/email-verify/email-verify.module').then( m => m.EmailVerifyPageModule)
  },
  {
    path: 'password-reset',
    loadChildren: () => import('./pages/password-reset/password-reset.module').then( m => m.PasswordResetPageModule)
  },
  {
    path: 'password-reset-recovery',
    loadChildren: () => import('./pages/password-reset-recovery/password-reset-recovery.module').then( m => m.PasswordResetRecoveryPageModule)
  },
  {
    path: 'password-reset-renew',
    loadChildren: () => import('./pages/password-reset-renew/password-reset-renew.module').then( m => m.PasswordResetRenewPageModule)
  },
  {
    path: 'password-reset-success',
    loadChildren: () => import('./pages/password-reset-success/password-reset-success.module').then( m => m.PasswordResetSuccessPageModule)
  },
  {
    path: 'test',
    loadChildren: () => import('./pages/test/test.module').then( m => m.TestPageModule)
  },
  {
    path: 'board',
    loadChildren: () => import('./pages/board/board.module').then( m => m.BoardPageModule)
  },
  {
    path: 'session-list',
    loadChildren: () => import('./pages/session-list/session-list.module').then( m => m.SessionListPageModule)
  },
  {
    path: 'session-edit',
    loadChildren: () => import('./pages/session-edit/session-edit.module').then( m => m.SessionEditPageModule)
  },
  {
    path: 'session-add',
    loadChildren: () => import('./pages/session-add/session-add.module').then( m => m.SessionAddPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
