import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private loader: HTMLIonLoadingElement;

  constructor(private loadingCtrl: LoadingController) { }

  async present() {
    this.loader = await this.loadingCtrl.create({
      message: 'Loading...',
      translucent: true
    });

    return await this.loader.present();
  }

  dismiss() {
    if (this.loader) {
      this.loader.dismiss().catch((e: any) => console.log('ERROR CATCH: LoadingController dismiss', e));
    }
  }
}
