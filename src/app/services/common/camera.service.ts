import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import * as AWS from 'aws-sdk';

declare const Buffer;
@Injectable({
  providedIn: 'root'
})
export class CameraService {

  configAWS = environment.AWS;

  private options: CameraOptions = {
    quality: 80,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    allowEdit: false,
    correctOrientation: true
  };

  constructor(private camera: Camera) { }

  takePhoto(sourceType): Promise<any> {
    return new Promise((resolve, reject) => {
      this.options.sourceType = sourceType;
      this.camera.getPicture(this.options).then((imageData) => {
        const base64Image = 'data:image/jpeg;base64,' + imageData;
        resolve(base64Image);
      }).catch((err) => {
        reject(err);
      });
    });
  }

  uploadImage(imageData) {
    return new Promise((resolve, reject) => {
      const body = Buffer.from(imageData.replace(/^data:image\/\w+;base64,/, ''), 'base64');
      const ext = imageData.split(';')[0].split('/')[1] || 'jpg';
      const date = Date.now();
      const key = date + '_' + this.getUID(8) + '.' + ext;

      this.s3Putimage({ body, mime: `image/${ext}` }, key, 'base64')
        .then((result) => { resolve(result); })
        .catch((err) => { reject(err); });
    });
  }

  private s3Putimage(file, key, encoding) {
    return new Promise((resolve, reject) => {
      const s3 = new AWS.S3({
        accessKeyId: this.configAWS.ACCESS_KEY_ID,
        secretAccessKey: this.configAWS.SECRET_ACCESS_KEY,
        region: this.configAWS.DEFAULT_REGION
      });

      const params = {
        Body: file.body,
        Bucket: this.configAWS.BUCKET,
        Key: this.configAWS.FOLDER + '/' + key,
        ACL: 'public-read',
        ContentType: file.mime,
        ContentEncoding: encoding
      };

      s3.putObject(params, (error, data) => {
        error ? reject(error) : resolve(this.generateLink(key));
      });

    });
  }

  private generateLink(key) {
    return this.configAWS.BASE_URL + this.configAWS.FOLDER + '/' + key;
  }

  getUID(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}
