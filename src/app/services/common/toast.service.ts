import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(public toastController: ToastController) { }

  async presentToast(msg: string) {
    const toast = await this
      .toastController
      .create({
        position: 'bottom',
        duration: 2000,
        message: msg
      });

    await toast.present();
  }

  async somethingWrong() {
    const toast = await this.toastController
      .create({
        position: 'top',
        duration: 2000,
        message: 'Something wrong. Please try again.'
      });

    await toast.present();
  }


}
