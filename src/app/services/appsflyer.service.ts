import { Injectable } from '@angular/core';
import { AppsflyerOptions, Appsflyer } from '@ionic-native/appsflyer/ngx';
import { environment } from 'src/environments/environment.prod';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AppsflyerService {
  // Is Appsflyer SDK initialized?
  private initialized = false;
  appsflyerConfig: AppsflyerOptions = environment.APPSFLYER_CONFIG;

  constructor(
    private platform: Platform,
    public appsflyer: Appsflyer,
  ) {
  }

  /**
   * initialize Appsflyer - add this to your app.component.ts
   */
  async init() {
    if (!this.platform.is('cordova')) { return; }

    if (!this.platform.is('ios')) {
      delete this.appsflyerConfig.appId;
    }

    try {
      console.log('appsflyer: options', this.appsflyerConfig);
      const data = await this.appsflyer.initSdk(this.appsflyerConfig);
      console.log('appsflyer: Data', data);
      this.initialized = true;
    } catch (error) {
      console.log('appsflyer:  error', error);
    }
  }

  log_event(type: string, data: any) {
    if (!this.initialized) { return; }
    this.appsflyer.trackEvent(type, data);
  }

  log_screen(page: string) {
    if (!this.initialized) { return; }
    this.appsflyer.trackEvent(page, {});
  }
}
