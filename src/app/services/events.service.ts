import { Injectable } from '@angular/core';
export declare type EventHandler = (...args: any[]) => any;

@Injectable({
  providedIn: 'root'
})

export class EventsService {
  constructor() { }
  /**
   * Subscribe to an event topic. Events that get posted to that topic will trigger the provided handler.
   *
   * @param topic the topic to subscribe to
   * @param handler the event handler
   */
  public subscribe(topic: string, handlers: EventHandler) {
    window.addEventListener(topic, handlers);
  }

  /**
   * Unsubscribe from the given topic. Your handler will no longer receive events published to this topic.
   *
   * @param topic the topic to unsubscribe from
   * @param handler the event handler
   *
   * @return true if a handler was removed
   */
  public unsubscribe(topic: string, handler?: EventHandler) {
    window.removeEventListener(topic, handler);
  }

  /**
   * Publish an event to the given topic.
   *
   * @param topic the topic to publish to
   * @param eventData the data to send as the event
   */
  publish(topic: string, args = {}) {
    window.dispatchEvent(new CustomEvent(topic, { detail: args }));
  }
}

