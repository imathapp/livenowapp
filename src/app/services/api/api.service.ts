import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { ResponseApi } from '../../models/index.model';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl = environment.BASE_URL;
  constructor(
    private http: HttpClient
  ) { }

  public getMeeting(data: any = {}) {
    const url = this.baseUrl + 'meeting/checkcode';
    console.log(data);
    return this.http.post<ResponseApi>(url, data);
  }

  // tslint:disable-next-line: variable-name
  public getMeetingDetail(meeting_id: any) {
    const url = this.baseUrl + 'meeting/' + meeting_id;
    return this.http.get<ResponseApi>(url);
  }

  // ===Session===
  public getSession(id) {
    const url = this.baseUrl + 'session/' + id;
    return this.http.get<ResponseApi>(url);
  }

  public createSession(data: any = {}) {
    const url = this.baseUrl + 'session';
    return this.http.post<ResponseApi>(url, data);
  }

  public updateSession(data, id) {
    const url = this.baseUrl + 'session/' + id;
    return this.http.put<ResponseApi>(url, data);
  }

  // ===BOARD===
  public createBoard(data) {
    const url = this.baseUrl + 'board';
    return this.http.post<ResponseApi>(url, data);
  }

  public getBoard(id) {
    const url = this.baseUrl + 'board/' + id;
    return this.http.get<ResponseApi>(url);
  }

  public updateBoard(data, id) {
    const url = this.baseUrl + 'board/' + id;
    return this.http.put<ResponseApi>(url, data);
  }

  public removeBoard(id) {
    const url = this.baseUrl + 'board/' + id;
    return this.http.delete<ResponseApi>(url);
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    // this.messageService.add(`HeroService: ${message}`);
  }


}
