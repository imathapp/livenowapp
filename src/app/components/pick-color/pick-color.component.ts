import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-pick-color',
  templateUrl: './pick-color.component.html',
  styleUrls: ['./pick-color.component.scss'],
})
export class PickColorComponent implements OnInit {
  colors = ['#000000', '#FFA500', '#9AFF00', '#00FF25', '#00FFE5', '#005AFF', '#6500FF', '#FF00D9'];
  constructor(
    private popoverCtrl: PopoverController
  ) {
  }

  async select(color) {
    await this.popoverCtrl.dismiss({ color });
  }
  ngOnInit() { }

}
