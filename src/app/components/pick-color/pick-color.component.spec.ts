import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PickColorComponent } from './pick-color.component';

describe('PickColorComponent', () => {
  let component: PickColorComponent;
  let fixture: ComponentFixture<PickColorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickColorComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PickColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
