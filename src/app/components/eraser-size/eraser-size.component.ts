import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-eraser-size',
  templateUrl: './eraser-size.component.html',
  styleUrls: ['./eraser-size.component.scss'],
})
export class EraserSizeComponent implements OnInit {

  constructor(
    private popoverCtrl: PopoverController
  ) {
  }

  async select(size) {
    await this.popoverCtrl.dismiss({ size });
  }

  ngOnInit() { }

}
