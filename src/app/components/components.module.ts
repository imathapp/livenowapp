import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { EraserSizeComponent } from './eraser-size/eraser-size.component';
import { PickColorComponent } from './pick-color/pick-color.component';

const PAGES_COMPONENTS = [
  EraserSizeComponent,
  PickColorComponent,
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
  ],
  declarations: PAGES_COMPONENTS,
  exports: PAGES_COMPONENTS,
  entryComponents: PAGES_COMPONENTS
})

export class ComponentsModule { }
