import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CheckLoginGuard implements CanLoad {
  constructor(
    private storage: Storage,
    private navCtrl: NavController
  ) { }

  async canLoad() {
    try {
      const hasLoggedIn = await this.storage.get('has_logged_in');
      if (hasLoggedIn) {
        this.navCtrl.navigateRoot('/app');
        return false;
      } else {
        this.navCtrl.navigateRoot('/login');
      }
      return true;
    } catch (error) {
      console.log(error);
    }
  }
}
