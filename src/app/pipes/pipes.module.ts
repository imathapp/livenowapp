import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeCounterPipe } from './time-counter.pipe';
import { MettingStatusPipe } from './metting-status.pipe';
import { LocalTimePipe } from './local-time.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TimeCounterPipe,
    MettingStatusPipe,
    LocalTimePipe
  ],
  exports: [
    TimeCounterPipe,
    MettingStatusPipe,
    LocalTimePipe
  ]
})
export class PipesModule { }
