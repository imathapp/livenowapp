import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeCounter'
})
export class TimeCounterPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const second = value % 60;
    value -= second;
    const min = (value / 60);
    return min + '\:' + second;
  }
}
