import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'localTime'
})
export class LocalTimePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const localTime = moment(value).utcOffset(args[0] * 60).format('YYYY-MM-DD HH:mm:ss');
    return localTime;
  }

}
