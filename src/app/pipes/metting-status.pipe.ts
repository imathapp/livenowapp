import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mettingStatus'
})
export class MettingStatusPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (value === 1) { return 'Not started'; }
    if (value === 2) { return 'Ongoing'; }
    if (value === 3) { return 'Finished'; }
  }

}
