// import { AuthService } from '../auth.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    // private auth: AuthService,
    private storage: Storage
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    return next.handle(req);
  }
  // intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // YOU CAN ALSO DO THIS
    // const token = this.authenticationService.getToke()

    // return from(this.storage.get(TOKEN_KEY))
    //   .pipe(
    //     switchMap(token => {
    //       if (token) {
    //         request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
    //       }

    //       if (!request.headers.has('Content-Type')) {
    //         request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    //       }

    //       if (this.debug) {
    //         request = request.clone({ url: this.url + request.url + '?XDEBUG_SESSION_START=1' });
    //       }

    //       return next.handle(request).pipe(
    //         map((event: HttpEvent<any>) => {
    //           if (event instanceof HttpResponse) {
    //             // do nothing for now
    //           }
    //           return event;
    //         }),
    //         catchError((error: HttpErrorResponse) => {
    //           const status = error.status;
    //           const reason = error && error.error.reason ? error.error.reason : '';

    //           return throwError(error);
    //         })
    //       );
    //     })
    //   );


  // }

  
}