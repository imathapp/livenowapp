import { Component, OnInit, Renderer2, OnDestroy, Injector } from '@angular/core';

import 'fabric-with-gestures';
import { Platform, PopoverController, ActionSheetController, ModalController, Events, AlertController } from '@ionic/angular';
import { Socket, SocketIoConfig } from 'ngx-socket-io';
import { ToastService } from 'src/app/services/common/toast.service';
import { CameraService } from 'src/app/services/common/camera.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { BoardModel, SessionModel } from 'src/app/models/index.model';
import { environment } from 'src/environments/environment.prod';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { EraserSizeComponent } from 'src/app/components/eraser-size/eraser-size.component';
import { CropImagePage } from 'src/app/modals/crop-image/crop-image.page';
import { LoadingService } from 'src/app/services/common/loading.service';
import { PickColorComponent } from 'src/app/components/pick-color/pick-color.component';
import { Storage } from '@ionic/storage';
import { BasePage } from '../base-page';


declare const fabric: any;
declare var OT: any;

@Component({
  selector: 'app-whiteboard',
  templateUrl: './whiteboard.page.html',
  styleUrls: ['./whiteboard.page.scss'],
})
export class WhiteboardPage extends BasePage implements OnInit, OnDestroy {

  constructor(
    injector: Injector,
    private socket: Socket,
    private renderer: Renderer2,
    private popoverCtrl: PopoverController,
    private actionSheetController: ActionSheetController,
    private cameraService: CameraService,
    private screenOrientation: ScreenOrientation,
    private apiService: ApiService,
    private alertController: AlertController
  ) {
    super(injector);
    this.session = {
      id: 1,
      uid: '446fe971-6ff9-4f9f-8876-746f3bad7170',
      opentok_session_id: '1_MX40NjQ0MjM4Mn5-MTU4MDM3MjEwMjk1MH5VeWt6cCtXRGZXY3JPdXc5SGZaZ3VGWEV-fg',
      opentok_token: 'T1==cGFydG5lcl9pZD00NjQ0MjM4MiZzaWc9MTI0MjJmM2ZjMmQyOWI4NjA0MmFiYTZhNjg2N2E4ZTEyNGE3Y2M0ZTpzZXNzaW9uX2lkPTFfTVg0ME5qUTBNak00TW41LU1UVTRNRE0zTWpFd01qazFNSDVWZVd0NmNDdFhSR1pYWTNKUGRYYzVTR1phWjNWR1dFVi1mZyZjcmVhdGVfdGltZT0xNTgwMzcyMTAyJnJvbGU9cHVibGlzaGVyJm5vbmNlPTE1ODAzNzIxMDIuOTg0NTEyNjc4MDQyNDg=',
      status: 0,
      board: {
        id: 1,
        json: null,
        width: 0,
        height: 0
      }
    };
    this.screen_id = this.session.board.id;
  }

  connectionId: any;
  timerCounter: any;
  // tslint:disable-next-line: variable-name
  screen_id: any;
  connected: any;
  isLeave: boolean;
  width: number;
  height: number;

  mainCanvas: any;
  colorPen = '#000000';
  widthPen = 2;
  widthErase = 4;
  colorErase = '#ffffff';
  mode = 'pointer';
  currentZoom = 1;
  showPicCanvas = false;

  contextMenu: any;
  objects: any[] = [];

  pausePanning: boolean;

  showingConfirmation = false;

  callSession: any;
  session: any;

  camSize = 50;
  publisher: any;
  subscriber: any;

  apiKey = environment.OPENTOK.API_KEY;
  actions = ['add', 'remove', 'modified', 'clear'];
  participantsNumber = 0;
  numUsrsInRoom = 0;
  publisherProperties = {
    width: this.camSize,
    height: this.camSize,
    resolution: '320x240',
    insertMode: 'append',
    device: 'iPad',
    name: 'Luong Tu'
  };

  async ngOnInit() {
    console.log('init');
    this.apiService.getSession(1).subscribe(res => {
      console.log(res);
      if (res.success) {
        this.session = res.data as SessionModel;
        console.log(this.session);
        
        this.initOpentokSession(this.session);
        this.initSocketSession(this.session);
        this.initMainCanvas();
        setTimeout(() => {
          this.loadScreen(this.session.board.id);
        }, 1000);
      }
    });
    // this.loadData();

    // try {
    //   if (!this.isMobileweb()) {
    //     this.setLandscape();
    //   } else {
    //     // delete when end develop
    //     const res = await this.loadSessionDetail();
    //     console.log(res);
    //     if (res.success) {
    //       this.session = res.data as SessionModel;
    //       this.initOpentokSession();
    //       this.initSocketSession(this.session);
    //       setTimeout(() => {
    //         this.loadScreen(this.session.screens[0]);
    //       }, 1000);
    //     } else {
    //       console.warn('FAIL');
    //     }

    //     this.initMainCanvas();
    //   }
    //   this.contextMenu = document.getElementById('contextMenu');
    // } catch (error) {
    // }

  }

  async ionViewDidEnter() {
    // await this.showLoadingView({ showOverlay: false });
  }

  loadData() {
    this.apiService.getSession(this.getParams().id).subscribe(res => {
      if (res.success) {
        this.session = res.data as SessionModel;
        this.initSocketSession(this.session);
        this.initOpentokSession(this.session);
        this.loadScreen(this.session.screens[0]);
      } else {
        console.warn('FAIL');
      }
    });
  }

  setLandscape() {
    if (this.isIos() || this.isAndroid()) {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE).then(() => {
        this.loadData();
      });
    }
  }

  initOpentokSession(session) {
    const { opentok_session_id: sessionId, opentok_token: token } = session;

    try {
      this.callSession = OT.initSession(this.apiKey, sessionId);
      this.publisher = OT.initPublisher('publisher');
      this.callSession.on({
        connectionCreated: () => {
          this.participantsNumber = ++this.numUsrsInRoom;
        },
        connectionDestroyed() {
          this.participantsNumber = --this.numUsrsInRoom;
        },
        sessionDisconnected() {
          this.numUsrsInRoom = 0;
        },
        sessionConnected: event => {
          console.log('sessionConnected');
          this.callSession.publish(this.publisher);
        },
      });

      this.callSession.connect(token, (err) => {
        if (err) {
          this.handleError(err);
        } else {
          this.publisher = OT.initPublisher('publisher', this.publisherProperties);
        }
      });
    } catch (error) {
      console.log(error);
    }
    console.log('init Opentok');
  }

  initSocketSession(session) {
    const config: SocketIoConfig = { url: environment.SOCKET_URL, options: { forceNew: true } };
    this.socket = new Socket(config);
    this.socket.connect();

    this.socket.emit('joinRoom', {
      session_id: session.id,
      meeting_id: session.meeting_id,
      username: session.display_name
    });
    this.initSocketEvents();
    console.log('init Socket');
  }

  initMainCanvas() {
    // set border selected object
    fabric.Object.prototype.set({
      objectCaching: !1,
      borderColor: '#09f',
      cornerColor: '#09f',
      transparentCorners: false,
      cornerStyle: 'circle'
    });

    fabric.Object.prototype.set({
      padding: 5,
      objectCaching: !1,
      borderColor: '#09f',
      cornerColor: '#09f',
      transparentCorners: !1,
      cornerStyle: 'circle',
      borderDashArray: [5, 5],
      lockUniScaling: !0,
      // rotatingPointOffset: 30
    });

    this.width = this.getWidth();
    this.height = this.getHeight();

    this.mainCanvas = new fabric.Canvas('mainCanvas', {
      selection: false,
      preserveObjectStacking: true,
      enableRetinaScaling: true,
      stopContextMenu: true,
      isDrawingMode: false,
      width: this.width,
      height: this.height
    });

    this.canvasEvents();
    console.log('init Canvas');
  }

  ngOnDestroy() {
    this.closeWindow();
  }

  async closeWindow() {
    if (this.subscriber && this.callSession) {
      this.callSession.unsubscribe(this.subscriber);
    }
    if (this.publisher && this.callSession) {
      this.callSession.unpublish(this.publisher);
    }
    if (this.callSession) {
      this.callSession.disconnect();
    }

    if (!this.isMobileweb) {
      await this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }

    if (this.socket) {
      this.socket.removeAllListeners();
      this.socket.disconnect(true);
    }

    this.router.navigate(['/home']);
  }

  enableCursor() {
    console.log('enableCursor');
    this.mode = 'pointer';
    this.pausePanning = false;
    this.mainCanvas.isDrawingMode = false;
    this.mainCanvas.renderAll();
  }

  enablePen() {
    console.log('enablePen');
    this.mode = 'pen';
    this.pausePanning = true;
    this.mainCanvas.isDrawingMode = true;
    this.mainCanvas.freeDrawingBrush.color = this.colorPen;
    this.mainCanvas.freeDrawingBrush.width = this.widthPen;
    this.mainCanvas.renderAll();
  }

  async selectColor(ev) {
    const popover = await this.popoverCtrl.create({
      component: PickColorComponent,
      event: ev,
      mode: 'ios',
      cssClass: 'pop-pick-color'
    });

    popover.onDidDismiss().then(data => {
      console.log(data);
      if (data.data) {
        this.colorPen = data.data.color;
        this.enablePen();
      }
    });

    return await popover.present();
  }

  async selectSizeEraser(ev) {
    const popover = await this.popoverCtrl.create({
      component: EraserSizeComponent,
      event: ev,
      mode: 'ios'
    });

    popover.onDidDismiss().then(data => {
      console.log(data);
      if (data.data) {
        this.widthErase = data.data.size;
        this.enableEraser();
      }
    });

    return await popover.present();
  }

  enableEraser() {
    this.mode = 'eraser';
    this.pausePanning = true;
    this.mainCanvas.isDrawingMode = true;
    this.mainCanvas.freeDrawingBrush.color = '#fff';
    this.mainCanvas.freeDrawingBrush.width = this.widthErase;
  }

  // TODO: implement later, only user who add that object can undo it
  undo() {
    if (this.objects.length === 0) { return; }
    const id = this.objects.pop();
    const objs = this.mainCanvas.getObjects();
    objs.forEach(obj => {
      if (obj.id === id) {
        this.mainCanvas.remove(obj).renderAll();
      }
    });
    this.publishChange('remove', { id });
  }

  async clearScreen() {
    const alert = await this.alertController.create({
      header: 'Are you sure',
      message: 'This action will clear whole screen and can\'t be undone',
      buttons: [
        {
          text: 'OK',
          handler: () => {
            if (this.mainCanvas) {
              this.clearCanvas();
              this.publishChange('clear', null);
            }
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    await alert.present();
  }

  clearCanvas() {
    if (this.mainCanvas) {
      this.objects = [];
      this.currentZoom = 1;
      this.mainCanvas.clear();
      this.mainCanvas.setViewportTransform([1, 0, 0, 1, 0, 0]);
    }
  }


  // canvas events
  canvasEvents() {
    this.enableEventGestures();
    this.mainCanvas.on('path:created', this.onPathCreated);

    this.mainCanvas.on('object:added', this.onObjectAdded);
    this.mainCanvas.on('object:moving', this.onObjectMoving);
    this.mainCanvas.on('object:scaling', this.onObjectScaling);
    this.mainCanvas.on('object:modified', this.onObjectModified);

    this.mainCanvas.on('mouse:down', this.onMouseDown);
    this.mainCanvas.on('mouse:up', this.onMouseUp);

    this.mainCanvas.on('selection:created', this.onSelectionCreated);
    this.mainCanvas.on('selection:updated', this.onSelectionUpdated);
    this.mainCanvas.on('selection:cleared', this.onSelectionCleared);

    this.mainCanvas.on('text:changed', this.onTextChanged);
  }

  onPathCreated = (e) => {
    this.publishChange('add', e.path);
  }

  onObjectAdded = (evt) => {
    console.log('object:added');
    if (!evt.target.id) {
      if (evt.target.type === 'path') {
        evt.target.set({
          id: this.uniqid(),
          selectable: false
        });
      } else {
        evt.target.set({
          id: this.uniqid()
        });
      }
    }

    this.objects.push(evt.target.id);
    this.mainCanvas.renderAll();
  }

  onObjectSelected = (evt) => {
    this.repositionContextMenu(evt.target);
  }

  onObjectMoving = (evt) => {
    this.repositionContextMenu(evt.target);
  }

  onObjectScaling = (evt) => {
    this.repositionContextMenu(evt.target);
  }

  onObjectModified = (evt) => {
    const obj = evt.target;
    obj.setCoords();
    this.mainCanvas.renderAll();

    const data = {
      id: obj.id,
      left: obj.left,
      top: obj.top,
      scaleX: obj.scaleX,
      scaleY: obj.scaleY,
      angle: obj.angle,
    };
    this.publishChange('modified', data);
  }

  onMouseDown = (evt) => {
    if (!this.mainCanvas.isDrawingMode) {
      this.mainCanvas.lastPosX = evt.pointer.x;
      this.mainCanvas.lastPosY = evt.pointer.y;
      this.pausePanning = false;
    }
  }

  onMouseUp = (evt) => {
    if (!this.mainCanvas.isDrawingMode) {
      this.publishChange('zoom', null);
    }
  }

  onSelectionCreated = (evt) => {
    this.disableEventGestures();
    this.repositionContextMenu(evt.target);
  }

  onSelectionUpdated = (evt) => {
    this.disableEventGestures();
    this.repositionContextMenu(evt.target);
  }

  onSelectionCleared = (evt) => {
    this.enableEventGestures();
    this.hideContextMenu();
  }

  onTextChanged(evt) {
    const obj = evt.target;
    this.mainCanvas.renderAll();
    const data = {
      id: obj.id,
      text: obj.text
    };
    this.publishChange('textchange', data);
  }

  enableEventGestures() {
    this.mainCanvas.on('touch:gesture', this.onTouchGesture);
    this.mainCanvas.on('touch:drag', this.onTouchDrag);
  }

  disableEventGestures() {
    this.mainCanvas.off('touch:gesture');
    this.mainCanvas.off('touch:drag');
  }

  onTouchGesture = (evt) => {
    this.pausePanning = true;
    if (evt.e.type === 'touchstart') {
      this.currentZoom = this.mainCanvas.getZoom();
    }

    let scale = this.currentZoom * evt.self.scale;
    scale = (scale <= 0.2) ? 0.2 : (scale >= 3 ? 3 : scale);

    const point = new fabric.Point(evt.self.x, evt.self.y);
    this.mainCanvas.zoomToPoint(point, scale);
    // this.publishChange('zoom', null);
  }

  onTouchDrag = (evt) => {
    if (this.pausePanning === false && evt.self.x !== undefined && evt.self.y !== undefined) {
      const vpt = this.mainCanvas.viewportTransform;
      vpt[4] += evt.self.x - this.mainCanvas.lastPosX;
      vpt[5] += evt.self.y - this.mainCanvas.lastPosY;
      this.mainCanvas.setViewportTransform(vpt);

      this.mainCanvas.lastPosX = evt.self.x;
      this.mainCanvas.lastPosY = evt.self.y;
    }
  }

  uniqid() {
    return (new Date().getTime() + Math.floor(1E4 * Math.random() + 1)).toString(16);
  }

  // Socket events
  initSocketEvents() {
    this.socket.fromEvent('userJoinRoom').subscribe((data: any) => {
      this.showToast(`${data.username} is now in session`);
    });

    this.socket.fromEvent('userLeftRoom').subscribe((data: any) => {
      this.showToast(`${data.name} has left room`);
    });

    this.socket.fromEvent('clocktick').subscribe((data: any) => {
      this.timerCounter = data.duration;
    });

    this.socket.fromEvent('drawing-other').subscribe((data: any) => {
      console.log('drawing-other');
      switch (data.action) {
        case 'add': {
          this.subscribeObjectAdded(data);
          break;
        }
        case 'remove': {
          this.subscribeObjectRemoved(data);
          break;
        }
        case 'modified': {
          this.subscribeObjectModified(data);
          break;
        }
        case 'clear': {
          this.subscribeCanvasCleared();
          break;
        }
        case 'screen': {
          this.subscribeScreenChanged(data);
          break;
        }
        case 'newpage': {
          this.subscribeNewPage(data);
          break;
        }
        case 'removepage': {
          this.subscribeTextChanged(data);
          break;
        }
        case 'textchange': {
          this.subscribeTextChanged(data);
          break;
        }
        default: {
          this.mainCanvas.setZoom(data.zoom);
          this.mainCanvas.setViewportTransform(data.viewportTransform);
          break;
        }
      }
    });
  }

  subscribeObjectAdded(data) {
    const object = JSON.parse(data.content);
    fabric.util.enlivenObjects([object], fabricObjects => {
      fabricObjects.forEach(fabricObject => {
        fabricObject.setCoords();
        if (data.object_id) {
          fabricObject.set({
            id: data.object_id,
            selectable: false
          });
        }
        this.mainCanvas.add(fabricObject);
        this.mainCanvas.renderAll();
      });
    });
  }

  subscribeObjectRemoved(data) {
    console.log('onRemovedObject');
    this.mainCanvas.forEachObject((obj) => {
      if (obj.id === data.object_id) {
        this.mainCanvas.remove(obj).renderAll();
        return false;
      }
    });
  }

  subscribeCanvasCleared() {
    this.clearCanvas();
  }

  subscribeObjectModified(data) {
    this.mainCanvas.forEachObject((obj) => {
      if (obj.id === data.object_id) {
        obj.animate({
          left: data.left,
          top: data.top,
          scaleX: data.scaleX,
          scaleY: data.scaleY,
          angle: data.angle
        }, {
          duration: 500,
          onChange: () => {
            obj.setCoords();
            this.mainCanvas.requestRenderAll();
          },
          easing: fabric.util.ease.easeOutExpo
        });
        return false;
      }
    });
  }

  async subscribeScreenChanged(data) {
    if (data.screen_id !== this.screen_id) {
      try {
        const res = await this.apiService.getBoard(data.screen_id).toPromise();
        if (res.success) {
          this.clearCanvas();
          this.screen_id = data.screen_id;
          this.mainCanvas.loadFromJSON(data, () => {
            this.mainCanvas.renderAll();
          });
        }
      } catch (error) {
        console.log(error);
      }
      setTimeout(() => {
        this.enableCursor();
      }, 1000);
    }
  }

  async subscribeNewPage(data) {
    try {
      const res = await this.apiService.getSession(this.session.id).toPromise();
      if (res.success) {
        this.clearCanvas();
        this.session = res.data;
        this.showPicCanvas = false;
        this.screen_id = data.screen_id;
      }
    } catch (error) {
      console.log(error);
      // this.toast.somethingWrong();
    }
  }

  async onRemovePage(data) {
    try {
      const res = await this.apiService.getSession(this.session.id).toPromise();
      if (res.success) {
        this.session = res.data;
        this.loadScreen(this.session.screens[0]);
      }
    } catch (error) {
      // this.toast.somethingWrong();
      console.log(error);
    }
  }

  subscribeTextChanged(data) {
    this.mainCanvas.forEachObject((obj) => {
      if (obj.id === data.object_id) {
        obj.set({ text: data.text });
        this.mainCanvas.renderAll();
        return false;
      }
    });
  }

  publishChange(action: string, data: any) {
    switch (action) {
      case 'add': {
        this.publishObjectAdded(action, data);
        break;
      }
      case 'remove': {
        this.publishObjectRemoved(action, data);
        break;
      }
      case 'modified': {
        this.publishObjectModified(action, data);
        break;
      }
      case 'clear': {
        this.socket.emit('drawing', { action });
        break;
      }
      case 'zoom': {
        this.publishCanvasZoom(action);
        break;
      }
      case 'screen': {
        this.publishScreenChanged(action, data);
        break;
      }
      case 'newpage': {
        this.publishNewPage(action, data);
        break;
      }
      case 'removepage': {
        this.publishRemovePage(action);
        break;
      }
    }

    if (this.actions.includes(action)) {
      const json = JSON.stringify(this.mainCanvas.toJSON(['selectable', 'id']));
      this.apiService.updateBoard({ json }, this.screen_id).subscribe(res => {
        console.log('Screen has been saved');
      }, error => {
        console.log(error);
      });
    }
  }

  publishObjectAdded(action: string, data: any) {
    const jsonData = data ? JSON.stringify(data) : null;
    this.objects.push(data.id);
    const dataSend = {
      action,
      object_id: data.id,
      content: jsonData
    };
    this.socket.emit('drawing', dataSend);
  }

  publishObjectRemoved(action: string, data: any) {
    const dataSend = {
      action,
      object_id: data.id
    };
    this.socket.emit('drawing', dataSend);
  }

  publishObjectModified(action: string, data: any) {
    const jsonData = data ? JSON.stringify(data) : null;
    const dataSend = {
      action,
      object_id: data.id,
      content: jsonData,
      zoom: this.mainCanvas.getZoom(),
      viewportTransform: this.mainCanvas.viewportTransform,
      left: data.left,
      top: data.top,
      scaleX: data.scaleX,
      scaleY: data.scaleY,
      angle: data.angle,
    };
    this.socket.emit('drawing', dataSend);
  }

  publishCanvasZoom(action: string) {
    const dataSend = {
      action,
      zoom: this.mainCanvas.getZoom(),
      viewportTransform: this.mainCanvas.viewportTransform
    };
    this.socket.emit('drawing', dataSend);
  }

  publishScreenChanged(action: string, data: any) {
    const dataSend = {
      action,
      screen_id: data.screen_id
    };
    this.socket.emit('drawing', dataSend);
  }

  publishNewPage(action: string, data: any) {
    const dataSend = {
      action,
      screen_id: data.screen_id
    };
    this.socket.emit('drawing', dataSend);
  }

  publishRemovePage(action: string) {
    const dataSend = {
      action
    };
    this.socket.emit('drawing', dataSend);
  }

  // END SOCKET EVENT

  addPic() {
    this.presentActionSheet();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      buttons: [{
        text: 'Camera',
        icon: 'camera',
        handler: () => {
          this.takePhoto(1);
        }
      }, {
        text: 'Photo library',
        icon: 'image',
        handler: () => {
          this.takePhoto(0);
        }
      }, {
        text: 'Test',
        icon: 'image',
        handler: () => {
          this.testImage();
        }
      }
        , {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

  testImage() {
    const url = 'http://fabricjs.com/assets/pug.jpg';
    fabric.Image.fromURL(url + '?t=' + (new Date().getTime()), img => {
      img.set({
        scaleX: .3,
        scaleY: .3,
        selectable: true
      });
      this.mainCanvas.add(img).setActiveObject(img);
      this.publishChange('add', img);
      this.enableCursor();
    }, { crossOrigin: 'anonymous' });
  }

  // modalCrop: Modal
  async takePhoto(sourceType) {
    try {
      this.isLeave = false;
      const modal = await this.modalCtrl.create({
        component: CropImagePage,
        componentProps: {
          type: 'whiteBoard'
        }
      });
      const image = await this.cameraService.takePhoto(sourceType);
      modal.onDidDismiss().then((data: any) => {
        this.isLeave = true;
        if (data.data) {
          this.addImage(data.data);
        }
      });
      modal.present().then(() => {
        this.events.publish('uploadImage', { img: image });
      });
    } catch (error) {
      console.log(error);
      this.isLeave = true;
    }
  }

  addImage = (data) => {
    fabric.Image.fromURL(data.imageData, img => {
      img.set({
        scaleX: .3,
        scaleY: .3
      });
      this.mainCanvas.add(img).setActiveObject(img);
      this.publishChange('add', img);
      this.enableCursor();
    }, { crossOrigin: 'anonymous' });
  }

  // END CAMERA
  repositionContextMenu(obj) {
    obj.setCoords();
    const b = 6;
    const c = obj.oCoords.bl.y + b;
    const d = obj.angle;
    const e = this.contextMenu.offsetWidth;
    const f = obj.getScaledWidth() * this.mainCanvas.getZoom();

    const data = {
      left: `${(f - e) / 2 + obj.oCoords.bl.x}px`,
      top: `${c}px`,
      transform: `rotate(${d}deg)`,
      transformOrigin: `${(e / 2 - f / 2 - b)}px`
    };

    this.renderer.setStyle(this.contextMenu, 'display', 'block');
    this.renderer.setStyle(this.contextMenu, 'left', data.left);
    this.renderer.setStyle(this.contextMenu, 'top', data.top);
    this.renderer.setStyle(this.contextMenu, 'transform', data.transform);
    this.renderer.setStyle(this.contextMenu, 'transformOrigin', data.transformOrigin);
  }

  hideContextMenu() {
    this.renderer.setStyle(this.contextMenu, 'display', 'none');
  }

  getActive() {
    return this.mainCanvas.getActiveObject();
  }

  objRemove() {
    const obj = this.getActive();
    const id = obj.id;
    this.mainCanvas.remove(obj).renderAll();
    this.hideContextMenu();
    const index = this.objects.indexOf(id);
    if (index > -1) {
      this.objects.splice(index, 1);
    }
    this.publishChange('remove', { id });
  }

  objLock() {
    const obj = this.getActive();
    if (obj === null) {
      return;
    }
    obj.set('locked', true);
    obj.set('selectable', false);
    this.mainCanvas.selection = false;
    this.mainCanvas.discardActiveObject().renderAll();
    this.hideContextMenu();
  }

  async loadScreen(screenId) {
    try {
      const res = await this.apiService.getBoard(screenId).toPromise();
      if (res.success) {
        this.clearCanvas();
        this.screen_id = screenId;
        this.publishChange('screen', { screen_id: screenId });
        this.mainCanvas.loadFromJSON(JSON.parse(res.data.json), (o, obj) => {
          this.mainCanvas.renderAll();
        });
      }
    } catch (error) {
      console.log(error);
    }

    setTimeout(() => {
      this.enableCursor();
    }, 1000);
  }

  async onPressingItem($event, screenId) {
    const alert = await this.alertController.create({
      header: 'Delete screen',
      subHeader: 'This action can not be reverted',
      buttons: [
        {
          text: 'Delete',
          handler: () => {
            this.deleteScreen(screenId);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteScreen(screenId) {
    try {
      const res = await this.apiService.removeBoard(screenId).toPromise();
      if (res.success) {
        const res2 = await this.apiService.getSession(this.session.id).toPromise();
        if (res2.success) {
          this.session = res2.data;
          this.loadScreen(this.session.screens[0]);
        }
        this.publishChange('removepage', null);
      } else {
        console.log('error');
      }
    } catch (error) {
      console.log(error);
    }
  }

  handleError(error) {
    if (error) {
      alert(error.message);
    }
  }

  picture() {
    fabric.Image.fromURL('assets/img8.jpg', img => {
      img.set({
        id: this.uniqid(),
        lockRotation: true,
        hasRotatingPoint: false,
        selectable: true,
        scaleX: .1,
        scaleY: .1
      });
      console.log(img);
      this.mainCanvas.add(img);
      this.publishChange('add', img);
    }, { crossOrigin: 'anonymous' });
  }

  async addNewScreen() {
    const data = {
      meeting_id: this.session.meeting_id,
      session_id: this.session.id,
      width: this.width,
      height: this.height
    };

    try {
      const res = await this.apiService.createBoard(data).toPromise();
      if (res.success) {
        const res2 = await this.apiService.getSession(this.session.id).toPromise();
        if (res2.success) {
          this.clearCanvas();
          this.session = res2.data;
          this.showPicCanvas = false;
          const lastIndex = this.session.screens.length - 1;
          this.screen_id = this.session.screens[lastIndex];
          this.publishChange('newpage', { screen_id: this.screen_id });
        }
      } else {
        console.log('error');
      }
    } catch (error) {
      console.log(error);
    }

  }

}
