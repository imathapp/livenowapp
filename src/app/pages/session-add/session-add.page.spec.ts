import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SessionAddPage } from './session-add.page';

describe('SessionAddPage', () => {
  let component: SessionAddPage;
  let fixture: ComponentFixture<SessionAddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionAddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SessionAddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
