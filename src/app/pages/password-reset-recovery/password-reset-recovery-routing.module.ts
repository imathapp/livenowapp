import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordResetRecoveryPage } from './password-reset-recovery.page';

const routes: Routes = [
  {
    path: '',
    component: PasswordResetRecoveryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordResetRecoveryPageRoutingModule {}
