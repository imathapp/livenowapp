import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordResetRecoveryPageRoutingModule } from './password-reset-recovery-routing.module';

import { PasswordResetRecoveryPage } from './password-reset-recovery.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordResetRecoveryPageRoutingModule
  ],
  declarations: [PasswordResetRecoveryPage]
})
export class PasswordResetRecoveryPageModule {}
