import { Injector } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import {
  LoadingController, ToastController,
  AlertController, IonInfiniteScroll, IonRefresher, Platform, Events, NavController, ModalController
} from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { EventsService } from '../services/events.service';

export abstract class BasePage {

  public isErrorViewVisible: boolean;
  public isEmptyViewVisible: boolean;
  public isContentViewVisible: boolean;
  public isLoadingViewVisible: boolean;

  protected refresher: IonRefresher;
  protected infiniteScroll: IonInfiniteScroll;
  protected navParams: ActivatedRoute;
  protected router: Router;
  protected events: EventsService;
  protected modalCtrl: ModalController;

  private loader: any;
  private navCtrl: NavController;
  private toastCtrl: ToastController;
  private loadingCtrl: LoadingController;
  private alertCtrl: AlertController;
  private platform: Platform;

  private activatedRoute: ActivatedRoute;

  private mCurrentPath: string;

  constructor(injector: Injector) {
    this.router = injector.get(Router);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.navCtrl = injector.get(NavController);
    this.loadingCtrl = injector.get(LoadingController);
    this.toastCtrl = injector.get(ToastController);
    this.alertCtrl = injector.get(AlertController);
    this.navParams = injector.get(ActivatedRoute);
    this.events = injector.get(EventsService);
    this.platform = injector.get(Platform);
    this.modalCtrl = injector.get(ModalController);

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // console.log(event);
        // console.log(NavigationEnd);
        // console.log(this.router.url);
        this.mCurrentPath = this.router.url.split('?')[0];
      }
    });
  }

  // abstract enableMenuSwipe(): boolean;

  public get currentPath(): string {
    return this.mCurrentPath;
  }

  // public get appUrl(): string {
  //   return environment.appUrl;
  // }

  // public get appImageUrl(): string {
  //   return environment.appImageUrl;
  // }

  // public getShareUrl(slug: string) {
  //   return this.appUrl + '/' + slug;
  // }

  async showLoadingView(params: { showOverlay: boolean }) {

    this.isErrorViewVisible = false;
    this.isEmptyViewVisible = false;
    this.isContentViewVisible = false;
    this.isLoadingViewVisible = true;

    if (params.showOverlay) {
      const loadingText = 'Loading...';

      this.loader = await this.loadingCtrl.create({
        message: loadingText
      });
      return await this.loader.present();
    }

    return true;

  }

  async dismissLoadingView() {

    if (!this.loader) { return; }

    try {
      await this.loader.dismiss();
    } catch (error) {
      console.log('ERROR: LoadingController dismiss', error);
    }
  }

  showContentView() {

    this.isErrorViewVisible = false;
    this.isEmptyViewVisible = false;
    this.isLoadingViewVisible = false;
    this.isContentViewVisible = true;

    this.dismissLoadingView();
  }

  showEmptyView() {

    this.isErrorViewVisible = false;
    this.isLoadingViewVisible = false;
    this.isContentViewVisible = false;
    this.isEmptyViewVisible = true;

    this.dismissLoadingView();
  }

  showErrorView() {

    this.isLoadingViewVisible = false;
    this.isContentViewVisible = false;
    this.isEmptyViewVisible = false;
    this.isErrorViewVisible = true;

    this.dismissLoadingView();
  }

  onRefreshComplete(data = null) {

    if (this.refresher) {
      this.refresher.complete();
    }

    if (this.infiniteScroll) {
      this.infiniteScroll.complete();

      if (data && data.length === 0) {
        this.infiniteScroll.disabled = true;
      } else {
        this.infiniteScroll.disabled = false;
      }
    }
  }

  async showToast(message: string) {
    const toast = await this.toastCtrl.create({
      message,
      color: 'primary',
      position: 'bottom',
      duration: 3000
    });

    return await toast.present();
  }

  async showAlert(message: string) {

    const okText = 'OK';

    const alert = await this.alertCtrl.create({
      header: '',
      message,
      buttons: [{
        text: okText,
        role: ''
      }]
    });

    return await alert.present();
  }

  async showConfirm(message: string) {

    const trans = { OK: 'OK', CANCEL: 'CANCEL' };

    const confirm = await this.alertCtrl.create({
      header: '',
      message,
      buttons: [{
        text: trans.CANCEL,
        role: 'cancel',
      }, {
        text: trans.OK,
        role: ''
      }]
    });

    return await confirm.present();
  }

  isIos(): boolean {
    return this.platform.is('ios');
  }

  isAndroid(): boolean {
    return this.platform.is('android');
  }

  isHybrid(): boolean {
    return this.platform.is('hybrid');
  }

  isPwa(): boolean {
    return this.platform.is('pwa');
  }

  isMobile(): boolean {
    return this.platform.is('mobile');
  }

  isMobileweb() {
    return this.platform.is('mobileweb');
  }

  isIpad() {
    return this.platform.is('ipad');
  }

  getWidth() {
    return this.platform.width();
  }

  getHeight() {
    return this.platform.height();
  }

  setRoot(url: string) {
    this.navCtrl.setDirection('root', false);
    this.router.navigateByUrl(url);
  }

  navigateTo(page: any, queryParams: any = {}) {
    this.router.navigate([page], { queryParams });
  }

  getParams() {
    return this.activatedRoute.snapshot.params;
  }

  getQueryParams() {
    return this.activatedRoute.snapshot.queryParams;
  }

}
