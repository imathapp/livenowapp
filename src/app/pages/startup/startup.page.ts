import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-startup',
  templateUrl: './startup.page.html',
  styleUrls: ['./startup.page.scss'],
})
export class StartupPage extends BasePage implements OnInit {
  session: any;
  constructor(
    injector: Injector,
  ) {
    super(injector);
  }

  ngOnInit() {
  }

}
