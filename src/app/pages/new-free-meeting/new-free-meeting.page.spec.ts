import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewFreeMeetingPage } from './new-free-meeting.page';

describe('NewFreeMeetingPage', () => {
  let component: NewFreeMeetingPage;
  let fixture: ComponentFixture<NewFreeMeetingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFreeMeetingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewFreeMeetingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
