import { Component, OnInit } from '@angular/core';
import { AppsflyerService } from 'src/app/services/appsflyer.service';
import * as moment from 'moment';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-free-meeting',
  templateUrl: './new-free-meeting.page.html',
  styleUrls: ['./new-free-meeting.page.scss'],
})
export class NewFreeMeetingPage implements OnInit {
  minDate: any;
  maxDate: any;
  public form: FormGroup;
  isLoadingViewVisible = false;

  constructor(
    private track: AppsflyerService
  ) {
    this.minDate = moment().format('YYYY-MM-DD');
    this.maxDate = moment().add(1, 'years').format('YYYY-MM-DD');
  }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      time: new FormControl('', Validators.required),
    });
    this.track.log_screen('new_free_meeting_page');
  }

  onSubmit() {
    const time = this.form.value.time ? moment(this.form.value.time).format('YYYY-MM-DD HH:mm:00') : '';
    console.log(time);
  }

}
