import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewFreeMeetingPageRoutingModule } from './new-free-meeting-routing.module';

import { NewFreeMeetingPage } from './new-free-meeting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewFreeMeetingPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [NewFreeMeetingPage]
})
export class NewFreeMeetingPageModule {}
