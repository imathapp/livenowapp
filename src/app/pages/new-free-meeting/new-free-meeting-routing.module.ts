import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewFreeMeetingPage } from './new-free-meeting.page';

const routes: Routes = [
  {
    path: '',
    component: NewFreeMeetingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewFreeMeetingPageRoutingModule {}
