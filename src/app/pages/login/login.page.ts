import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BasePage } from '../base-page';
import { Events } from '@ionic/angular';
import { UserService } from 'src/app/services/api/user.service';
import { ForgotPasswordPage } from '../forgot-password/forgot-password.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {
  public form: FormGroup;
  public isLoadingByUsername = false;
  constructor(
    injector: Injector,
    private userService: UserService,
  ) {
    super(injector);

  }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }


  async onLogin() {

    try {

      if (this.form.invalid) {
        const message = 'Please fill the required fields.';
        return this.showToast(message);
      }

      await this.showLoadingView({ showOverlay: false });
      this.isLoadingByUsername = true;

      let user = await this.userService.signIn(this.form.value);

      this.showContentView();
      this.isLoadingByUsername = false;

      const str = `Logged in as {{ username }}`;
      this.showToast(str);

      this.onDismiss();

      this.events.publish('user:login', user);

    } catch (err) {

      if (err.code === 101) {
        const str = 'Invalid credentials';
        this.showToast(str);
      } else {
        const str = 'Network error';
        this.showToast(str);
      }

      this.showContentView();
      this.isLoadingByUsername = false;
    }
  }

  onDismiss() {
    this.modalCtrl.dismiss();
  }

  async onForgotPasswordButtonTouched() {
    const modal = await this.modalCtrl.create({
      component: ForgotPasswordPage,
    });

    return await modal.present();
  }
  async openSignup() {
    // this.router.navigate(['/signup']);
  }


}
