import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordResetRenewPage } from './password-reset-renew.page';

const routes: Routes = [
  {
    path: '',
    component: PasswordResetRenewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordResetRenewPageRoutingModule {}
