import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordResetRenewPageRoutingModule } from './password-reset-renew-routing.module';

import { PasswordResetRenewPage } from './password-reset-renew.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordResetRenewPageRoutingModule
  ],
  declarations: [PasswordResetRenewPage]
})
export class PasswordResetRenewPageModule {}
