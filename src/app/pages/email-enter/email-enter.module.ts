import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EmailEnterPageRoutingModule } from './email-enter-routing.module';

import { EmailEnterPage } from './email-enter.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EmailEnterPageRoutingModule
  ],
  declarations: [EmailEnterPage]
})
export class EmailEnterPageModule {}
