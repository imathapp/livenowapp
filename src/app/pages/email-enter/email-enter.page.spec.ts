import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmailEnterPage } from './email-enter.page';

describe('EmailEnterPage', () => {
  let component: EmailEnterPage;
  let fixture: ComponentFixture<EmailEnterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailEnterPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EmailEnterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
