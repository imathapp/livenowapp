import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmailEnterPage } from './email-enter.page';

const routes: Routes = [
  {
    path: '',
    component: EmailEnterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EmailEnterPageRoutingModule {}
