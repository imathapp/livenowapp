import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api/api.service';
import { AppsflyerService } from 'src/app/services/appsflyer.service';
import { NavParams, ModalController } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  meetingCode: string;
  passcodeForm: FormGroup;
  err: string;
  meetingId: string;
  validationMessages = {
    name: [
      { type: 'required', message: 'Name is required.' }
    ],
    code: [
      { type: 'required', message: 'Passcode is required.' },
      { type: 'minlength', message: 'Passcode must be at least 4 charecters long.' },
      { type: 'invalid', message: 'Passcode is invalid.' }
    ]
  };
  public isLoadingViewVisible: boolean;
  constructor(
    private router: Router,
    // private route: Route,
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private track: AppsflyerService,
    private activatedRoute: ActivatedRoute,
    private storage: Storage,
  ) {
  }
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
    // this.meetingId = this.navParams.get('meeing_id') || '';
    this.meetingId = this.activatedRoute.snapshot.paramMap.get('id') || '';
    console.log(this.meetingId);
    this.passcodeForm = this.formBuilder.group({
      name: new FormControl(this.meetingId, Validators.compose([
        Validators.required,
      ])),
      meeting_id: new FormControl(this.meetingId, Validators.compose([
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(9)
      ])),
      code: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4)
      ]))
    });
    this.storage.get('name').then(data => {
      data = data || '';
      this.passcodeForm.patchValue({
        name: data
      });
    });
    this.track.log_screen('home_page');
  }

  joinMeeting() {
    this.passcodeForm.setErrors({
      invalid: true
    });
    console.log(this.meetingCode);
  }

  async onSubmit() {
    const formData = this.passcodeForm.value;
    const res = await this.apiService.getMeeting(formData).toPromise();
    if (res.success) {
      this.router.navigate(['/whiteboard', res.data.id]);
    } else {
      this.err = res.message;
      console.log('Fail');
    }
  }

  async showPopoverMenu() {

  }

  signup() {
    this.router.navigate(['/signup']);
  }

  login() {
    this.router.navigate(['/login']);
  }

  newFreeMeeting() {
    this.router.navigate(['/new-free-meeting']);
  }

  logout() {
    // logout
  }

  goAccount() {
    this.router.navigate(['/account']);
  }

  goMeetingHistory() {
    this.router.navigate(['/meeting-history']);
  }

  
}
