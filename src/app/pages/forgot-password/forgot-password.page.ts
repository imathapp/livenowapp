import { Component, OnInit, Injector } from '@angular/core';
import { BasePage } from '../base-page';
import { UserService } from 'src/app/services/api/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage extends BasePage {

  public form: FormGroup;

  constructor(
    injector: Injector,
    private userService: UserService
  ) {
    super(injector);

    this.form = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  enableMenuSwipe() {
    return false;
  }

  onDismiss() {
    this.modalCtrl.dismiss();
  }

  async onSubmit() {

    // if (this.form.invalid) {
    //   const message = await this.getTrans('INVALID_FORM');
    //   return this.showToast(message);
    // }

    // try {

    //   await this.showLoadingView({ showOverlay: false });

    //   const email = this.form.value.email;

    //   await this.userService.recoverPassword(email);

    //   this.showContentView();

    //   this.translate.get('FORGOT_PASSWORD_SUCCESS')
    //     .subscribe((str: string) => this.showAlert(str));

    // } catch (err) {

    //   this.showContentView();

    //   this.translate.get('ERROR_NETWORK')
    //     .subscribe((str: string) => this.showToast(str));

    // }

  }

}
