import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-password-reset-success',
  templateUrl: './password-reset-success.page.html',
  styleUrls: ['./password-reset-success.page.scss'],
})
export class PasswordResetSuccessPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
