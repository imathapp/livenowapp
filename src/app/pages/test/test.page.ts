import { Component, OnInit, Injector, Renderer2 } from '@angular/core';
import anime from 'animejs';
import { BasePage } from '../base-page';
import { Socket, SocketIoConfig } from 'ngx-socket-io';
import { PopoverController, ActionSheetController, AlertController } from '@ionic/angular';
import { CameraService } from 'src/app/services/common/camera.service';
import { ApiService } from 'src/app/services/api/api.service';
import { SessionModel } from 'src/app/models/index.model';
import { environment } from 'src/environments/environment.prod';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

declare const fabric: any;
declare var OT: any;

@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})
export class TestPage extends BasePage implements OnInit {

  constructor(
    injector: Injector,
    private socket: Socket,
    private renderer: Renderer2,
    private popoverCtrl: PopoverController,
    private actionSheetController: ActionSheetController,
    private cameraService: CameraService,
    private screenOrientation: ScreenOrientation,
    private apiService: ApiService,
    private alertController: AlertController
  ) {
    super(injector);
    this.session = {
      id: 1111,
      meeting_id: 1111,
      display_name: 'Luong Tu',
      opentok_session_id: '2_MX40NjQ0MjM4Mn5-MTU3ODY0NDEyNzAyNn5IZ1NtTzZnNUpLSGlFenZjTHJ2ck1jTGV-fg',
      opentok_token: 'T1==cGFydG5lcl9pZD00NjQ0MjM4MiZzaWc9YjhiZTZjNzVlZDE0MDllNzc2YmIwMmYyM2QxODgyZDM1YTQxZTIwNTpzZXNzaW9uX2lkPTJfTVg0ME5qUTBNak00TW41LU1UVTNPRFkwTkRFeU56QXlObjVJWjFOdFR6Wm5OVXBMU0dsRmVuWmpUSEoyY2sxalRHVi1mZyZjcmVhdGVfdGltZT0xNTc4NjQ0MTU5JnJvbGU9cHVibGlzaGVyJm5vbmNlPTE1Nzg2NDQxNTkuMjE5NjEwMTkyNzU0OTkmZXhwaXJlX3RpbWU9MTU4MTIzNjE1OQ=='
    };
  }

  private connected: any;
  private isLeave: boolean;
  private width: number;
  private height: number;
  private mainCanvas: any;
  colorPen = '#000000';
  widthPen = 2;
  widthErase = 4;
  colorErase = '#ffffff';
  mode = 'pointer';
  currentZoom = 1;
  contextMenu: any;
  objects: any[] = [];
  pausePanning: boolean;

  showingConfirmation = false;
  callSession: any;
  session: any;

  camSize = 50;
  publisher: any;
  subscriber: any;

  apiKey = environment.OPENTOK.API_KEY;
  actions = ['add', 'remove', 'modified', 'clear'];
  participantsNumber = 0;
  numUsrsInRoom = 0;
  publisherProperties = {
    width: this.camSize,
    height: this.camSize,
    resolution: '320x240',
    insertMode: 'append',
    device: 'iPad',
    name: 'Luong Tu'
  };


  isHide = false;

  ngOnInit() {
    console.log(anime);
  }

  initStream() {

  }

  initCanvas() {
    // set border selected object
    fabric.Object.prototype.set({
      objectCaching: !1,
      borderColor: '#09f',
      cornerColor: '#09f',
      transparentCorners: false,
      cornerStyle: 'circle'
    });

    fabric.Object.prototype.set({
      padding: 5,
      objectCaching: !1,
      borderColor: '#09f',
      cornerColor: '#09f',
      transparentCorners: !1,
      cornerStyle: 'circle',
      borderDashArray: [5, 5],
      lockUniScaling: !0,
      // rotatingPointOffset: 30
    });

    this.width = this.getWidth();
    this.height = this.getHeight();

    this.mainCanvas = new fabric.Canvas('mainCanvas', {
      selection: false,
      preserveObjectStacking: true,
      enableRetinaScaling: true,
      stopContextMenu: true,
      isDrawingMode: false,
      width: this.width,
      height: this.height
    });

    // this.canvasEvents();
  }

  initOpentok(session) {
    const { opentok_session_id: sessionId, opentok_token: token } = session;

    try {
      this.callSession = OT.initSession(this.apiKey, sessionId);
      this.publisher = OT.initPublisher('publisher');

      this.callSession.on({
        connectionCreated: () => {
          this.participantsNumber = ++this.numUsrsInRoom;
        },
        connectionDestroyed() {
          this.participantsNumber = --this.numUsrsInRoom;
        },
        sessionDisconnected() {
          this.numUsrsInRoom = 0;
        },
        sessionConnected: event => {
          console.log('sessionConnected');
          this.callSession.publish(this.publisher);
        },
      });

      this.callSession.connect(token, (err) => {
        if (err) {
          this.handleError(err);
        } else {
          this.publisher = OT.initPublisher('publisher', this.publisherProperties);
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  handleError(error) {
    if (error) {
      alert(error.message);
    }
  }

  initSocket(session) {
    const config: SocketIoConfig = { url: environment.SOCKET_URL, options: { forceNew: true } };
    this.socket = new Socket(config);
    this.socket.connect();

    this.socket.emit('joinRoom', {
      session_id: session.id,
      meeting_id: session.meeting_id,
      username: session.display_name
    });
    this.initSocketEvents();
  }

  canvasEvents() {
    // this.enableEventGestures();
    // this.mainCanvas.on('path:created', this.onPathCreated);

    // this.mainCanvas.on('object:added', this.onObjectAdded);
    // this.mainCanvas.on('object:moving', this.onObjectMoving);
    // this.mainCanvas.on('object:scaling', this.onObjectScaling);
    // this.mainCanvas.on('object:modified', this.onObjectModified);

    // this.mainCanvas.on('mouse:down', this.onMouseDown);
    // this.mainCanvas.on('mouse:up', this.onMouseUp);

    // this.mainCanvas.on('selection:created', this.onSelectionCreated);
    // this.mainCanvas.on('selection:updated', this.onSelectionUpdated);
    // this.mainCanvas.on('selection:cleared', this.onSelectionCleared);

    // this.mainCanvas.on('text:changed', this.onTextChanged);
  }

  initSocketEvents() {

  }

  ngOnDestroy() {
    this.closeWindow();
  }

  async closeWindow() {
    if (this.subscriber && this.callSession) {
      this.callSession.unsubscribe(this.subscriber);
    }
    if (this.publisher && this.callSession) {
      this.callSession.unpublish(this.publisher);
    }
    if (this.callSession) {
      this.callSession.disconnect();
    }

    if (!this.isMobileweb) {
      await this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }

    if (this.socket) {
      this.socket.removeAllListeners();
      this.socket.disconnect(true);
    }

    this.router.navigate(['/home']);
  }



  hideTools() {
    this.isHide = true;
    anime({
      targets: '.tools',
      translateY: 66,
      easing: 'easeOutExpo'
    });
  }

  showTools() {
    this.isHide = false;
    anime({
      targets: '.tools',
      translateY: 0,
      easing: 'easeOutExpo'
    });
  }

}
